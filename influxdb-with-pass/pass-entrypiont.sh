#!/bin/bash

influxd & PIDINFLUX=$!

sleep 2

curl --request POST --insecure  --url "https://localhost:8086/query?q=CREATE%20USER%20${INFLUX_USER}%20WITH%20PASSWORD%20'${INFLUX_PASSWORD}'%20WITH%20ALL%20PRIVILEGES"

# Could be a better way to do this but this will keep the container running until influxd stops
wait $PIDINFLUX