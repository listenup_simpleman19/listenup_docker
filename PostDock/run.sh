#!/bin/bash

USER="simpleman19"
MANAGER_IP='manager1.local'

scp docker-compose/postgres-10_repmgr-4.0_pgpool-3.7_barman-2.4.yml $MANAGER_IP:/tmp/postgres.yml

ssh -t $USER@$MANAGER_IP -C "cd /tmp && docker stack deploy -c postgres.yml postgres"
