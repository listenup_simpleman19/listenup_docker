#!/bin/bash

pushd easy-etcd || exit
./build.sh
popd || exit
pushd easy-lb-haproxy || exit
./build.sh
popd || exit
pushd redis-with-pass || exit
./build.sh
popd || exit
pushd influxdb-with-pass || exit
./build.sh
popd || exit
pushd psql-client || exit
./build.sh
popd || exit
pushd curl-client || exit
./build.sh
popd || exit

pushd PostDock || exit

# ./build.sh

popd || exit
