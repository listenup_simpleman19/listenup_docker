#!/bin/bash

push () {
  TAG="$(git describe --tags)"
  docker image tag $1 ${REGISTRY}/$1:$TAG
  docker push ${REGISTRY}/$1:$TAG

  TAG=latest
  docker image tag $1 ${REGISTRY}/$1:$TAG
  docker push ${REGISTRY}/$1:$TAG
}

push simpleman19/easy-etcd

push simpleman19/easy-lb-haproxy

push simpleman19/redis-with-pass

push listenup/influxdb-with-pass

push listenup/psql-client

push listenup/curl-client

#docker image tag listenup/barman ${REGISTRY}/listenup/barman
#docker push ${REGISTRY}/listenup/barman

#docker image tag listenup/pgpool ${REGISTRY}/listenup/pgpool
#docker push ${REGISTRY}/listenup/pgpool

#docker image tag listenup/postgres ${REGISTRY}/listenup/postgres
#docker push ${REGISTRY}/listenup/postgres
