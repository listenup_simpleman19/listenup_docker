#!/bin/bash
docker build -t listenup/barman ./src -f ./src/Barman-2.4-Postgres-10.Dockerfile

docker build -t listenup/pgpool ./src -f ./src/Pgpool-3.7-Postgres-10.Dockerfile

docker build -t listenup/postgres ./src -f ./src/Postgres-10-Repmgr-4.0.Dockerfile
